/**
 * @file
 * Settings for the FlipClock Library
 */
jQuery(document).ready(function() {
    var clock;
	// Grab the current date
	var currentDate = new Date();
	var futureDate = new Date(Drupal.settings.flipclock.flipclock_date * 1000);
	console.log(Drupal.settings);
	//var futureDate  = new Date(currentDate.getFullYear(), 10, 6);
	// Calculate the difference in seconds between the future and current date
	var diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;
	// Instantiate a coutdown FlipClock
	clock = jQuery('.clock').FlipClock(diff, {
		clockFace: 'DailyCounter',
		countdown: true,
		callbacks: {
			stop: function() {
				jQuery('.message').html('The clock has stopped!')
			}
		}
	});
});