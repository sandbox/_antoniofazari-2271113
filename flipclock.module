<?php
/**
 * @file
 * This module provides a block implementing FlipClock
 *
 * Author: Heath Naylor
 */

/**
 * Implements hook_theme().
 */
function flipclock_theme() {
  return array(
    'flipclock_container' => array(
      'variables' => array(),
    )
  );
}

/**
 * Implements hook_block_info().
 */
function flipclock_block_info() {
  $blocks = array();
  $blocks['flipclock'] = array(
    'info'  => t('Flipclock'),
    'cache' => DRUPAL_CACHE_GLOBAL
  );
  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function flipclock_block_configure($delta) {
  $form = array();
  switch ($delta) {
    case 'flipclock':
      module_load_include('inc', 'date_api', 'date_api_elements');
      $form['flipclock_date'] = array(
        '#type' => 'date_select',
        '#title' => t('Timer date'),
        '#default_value' => variable_get('flipclock_date', date('Y-m-d G:i:s')),
        '#date_format' => 'Y-m-d H:i',
      );
      break;
  }
  return $form;
}


/**
 * Implements hook_block_save().
 */
function flipclock_block_save($delta = '', $edit = array()) {
  switch ($delta) {
    case 'flipclock':
      variable_set('flipclock_date', $edit['flipclock_date']);
      break;
  }
}

/**
 * Implements hook_block_view().
 */
function flipclock_block_view($delta = '', $edit = array()) {
  $block = '';
  switch ($delta) {
    case 'flipclock':
      $block['subject'] = 'Countdown';
      $block['content'] = array(
        '#markup' => theme('flipclock_container', array()),
        '#attached' => flipclock_attach(),
      );
      break;
  }
  return $block;
}

/**
 * Adds javascript to the block.
 */
function flipclock_attach() {
  $attach = array();
  $path = drupal_get_path('module', 'flipclock');
  // add external js files
  $attach['js'] = array(
    $path . '/js/flipclock.min.js' => array(
      'type' => 'file',
      'scope' => 'footer',
    ),
    $path . '/js/flipclock.settings.js' => array(
      'type' => 'file',
      'scope' => 'footer',
    )
  );

  // add js settings
  $settings = array(
    'flipclock_date' => strtotime(variable_get('flipclock_date', date('Y-m-d G:i:s'))),
  );
  $attach['js'][] = array(
   'data' =>  array('flipclock' => $settings),
   'type' => 'setting',
  );

  //add css
  $attach['css'] = array(
   $path . '/css/flipclock.css',
  );

  return $attach;
}

/**
 * Returns HTML for the timer container.
 *
 * @param type $variables
 * @return string
 */

function theme_flipclock_container($variables) {
  $output = '';
  $output .= '<div class="clock" style="margin:0em;"></div>';
  $output .= '<div class="message"></div>';

  return $output;
}
